<?php
 class makhlukHidup{

    protected int $numberOfLegs;
    protected bool $canFly;
    protected string $foodClassification;
    protected bool $canSwim;
    protected string $respirationOrgan;
    
    function eat($foodClassification){
        echo "Termasuk hewan $foodClassification <br>";
    }
    
    function run($numberOfLegs){
        if($numberOfLegs <= 0){
            echo "Enggak bisa lari soalnya gk punya kaki <br>";
        }else{
            echo "Bisa lari soalnya punya $numberOfLegs kaki <br>";
        }
    }
    function swim($canSwim){
        if($canSwim){
            echo "Hewan ini lho bisa renang <br>";
        }else{
            echo "Hewan ini gk bisa renang <br>";
        }
    }
    function fly($canFly){
        if($canFly){
            echo "Hewan ini lho bisa terbang <br>";
        }else{
            echo "Hewan ini gk bisa terbang <br>";
        }
    }
    function cry(){
        echo "Semua makhluk hidup seharusnya bisa nangis <br>";
    }
    function ambekan($respirationOrgan){
        echo "Hewan ini bernafas menggunakan $respirationOrgan <br>";
    }
}

 class Catfish extends makhlukHidup{
    public $nama = "Iwak Lele";
 }

 class BettaFish extends makhlukHidup{
    public $nama = "Iwak Cupang";
 }

 class Crocodile extends makhlukHidup{
    public $nama = "Buaya";
 }

 class Aligator extends makhlukHidup{
    public $nama = "Aligator";
 }

 class Lizard extends makhlukHidup{
    public $nama = "Cicak";
 }

 class Snake extends makhlukHidup{
    public $nama = "Ular";
 }

 class Swan extends makhlukHidup{
    public $nama = "Angsa";
 }

 class Chicken extends makhlukHidup{
    public $nama = "Pitek";
 }

 class Swallow extends makhlukHidup{
    public $nama = "Sandal";
    
 }

 class Duck extends makhlukHidup{
    public $nama = "Bebek";   
    
 }

 class Human extends makhlukHidup{
    public $nama = "Manusia";
    
 }

 class Whale extends makhlukHidup{
    public $nama = "Paus";
 }

 class Dolphine extends makhlukHidup{
    public $nama = "Lumba-lumba";
 }

 class Bat extends makhlukHidup{
    public $nama = "Kelelawar";
 }

 class Tiger extends makhlukHidup{
    public $nama = "Macan";
 }

 class tampilHasil{
    private $hewan;
    function tampilLele(){
        $hewan = new Catfish();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(0);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Insang");
        echo "============================ <br> <br>";
    }

    function tampilCupang(){
        $hewan = new BettaFish();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(0);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Insang");
        echo "============================ <br> <br>";
    }

    function tampilBuaya(){
        $hewan = new Crocodile();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(4);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilAligator(){
        $hewan = new Aligator();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(4);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilCicak(){
        $hewan = new Lizard();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(4);
        $hewan->swim(false);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilUlar(){
        $hewan = new Snake();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(0);
        $hewan->swim(false);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilAngsa(){
        $hewan = new Swan();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(2);
        $hewan->swim(true);
        $hewan->fly(true);
        $hewan->cry();
        $hewan->ambekan("paru - paru & pundi-pundi udara");
        echo "============================ <br> <br>";
    }

    function tampilPitik(){
        $hewan = new Chicken();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(2);
        $hewan->swim(false);
        $hewan->fly(true);
        $hewan->cry();
        $hewan->ambekan("paru - paru & pundi-pundi udara");
        echo "============================ <br> <br>";
    }

    function tampilSandal(){
        $hewan = new Swallow();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Herbivora");
        $hewan->run(2);
        $hewan->swim(false);
        $hewan->fly(true);
        $hewan->cry();
        $hewan->ambekan("paru - paru & pundi-pundi udara");
        echo "============================ <br> <br>";
    }

    function tampilBebek(){
        $hewan = new Duck();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(2);
        $hewan->swim(true);
        $hewan->fly(true);
        $hewan->cry();
        $hewan->ambekan("paru - paru & pundi-pundi udara");
        echo "============================ <br> <br>";
    }

    function tampilManusia(){
        $hewan = new Human();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(2);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilPaus(){
        $hewan = new Whale();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(0);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilLumba(){
        $hewan = new Dolphine();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(0);
        $hewan->swim(true);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilKelelawar(){
        $hewan = new Bat();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Omnivora");
        $hewan->run(2);
        $hewan->swim(false);
        $hewan->fly(true);
        $hewan->cry();
        $hewan->ambekan("Paru - paru");
        echo "============================ <br> <br>";
    }

    function tampilMacan(){
        $hewan = new Tiger();
        echo "========== ". $hewan->nama. " ========== <br>";
        $hewan->eat("Karnivora");
        $hewan->run(4);
        $hewan->swim(false);
        $hewan->fly(false);
        $hewan->cry();
        $hewan->ambekan("Paru - paru");
        echo "============================ <br> <br>";
    }
    
 }

 $tampilHasil = new tampilHasil();
 $tampilHasil->tampilLele();
 $tampilHasil->tampilCupang();
 $tampilHasil->tampilBuaya();
 $tampilHasil->tampilAligator();
 $tampilHasil->tampilCicak();
 $tampilHasil->tampilUlar();
 $tampilHasil->tampilAngsa();
 $tampilHasil->tampilPitik();
 $tampilHasil->tampilSandal();
 $tampilHasil->tampilBebek();
 $tampilHasil->tampilManusia();
 $tampilHasil->tampilPaus();
 $tampilHasil->tampilLumba();
 $tampilHasil->tampilKelelawar();
 $tampilHasil->tampilMacan();


?>