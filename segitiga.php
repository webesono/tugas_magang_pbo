

<?php
class segitiga{
    private $alas;
    private $tinggi;

    function hitungLuas($alas, $tinggi): float{
        $luas = $alas*$tinggi/2;
        return $luas;
    }

    function hitungKeliling($alas, $tinggi): float{
        $sisilain = sqrt(pow($alas, 2) + pow($tinggi, 2));
        $keliling = $alas+$tinggi+$sisilain;
        return $keliling;
    }
 };

    $segitiga = new segitiga();
    echo "Luas segitiga adalah " .$segitiga->hitungLuas(3,4). "<br>";
    echo "Keliling segitiga adalah ". $segitiga->hitungKeliling(3,4);

?>