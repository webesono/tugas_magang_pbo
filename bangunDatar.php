<?php

interface abs_bangunDatar3{
    public function hitungKeliling($a, $b, $c) ;
    public function hitungLuas($a, $b, $c);
}

interface abs_bangunDatar2{
    public function hitungKeliling($a, $b) ;
    public function hitungLuas($a, $b);
}

interface abs_bangunDatar1{
    public function hitungKeliling($a) ;
    public function hitungLuas($a);
}

class Persegi implements abs_bangunDatar1{

    function hitungKeliling($sisi)  
    {
        return 4*$sisi;
    }
    function hitungLuas($sisi)
    {
        return $sisi * $sisi;
    }

}

class PersegiPanjang implements abs_bangunDatar2{

    function hitungKeliling($panjang, $lebar)  
    {
        return 2*($panjang+$lebar);
    }
    function hitungLuas($panjang, $lebar)
    {
        return $panjang * $lebar;
    }
}

class Segitiga implements abs_bangunDatar2{
    function hitungLuas($alas, $tinggi){
        return $alas*$tinggi/2;
    }

    function hitungKeliling($alas, $tinggi){
        $sisilain = sqrt(pow($alas, 2) + pow($tinggi, 2));
        return $alas+$tinggi+$sisilain;
    }

 }

 class Lingkaran implements abs_bangunDatar1{
    function hitungLuas($jarijari){
        return pi() * pow($jarijari,2);
    }

    function hitungKeliling($jarijari){
        return pi() * 2 * $jarijari;
    }
    
 }

 class Trapesium implements abs_bangunDatar3{
    function hitungLuas($atas, $bawah, $tinggi){
        return ($atas+$bawah)* $tinggi /2;
    }

    function hitungKeliling($atas, $bawah, $tinggi){
        $aminb = $bawah - $atas;
        $sisimiring = sqrt(pow($tinggi, 2) + pow($aminb, 2));
        return $atas+$bawah+$tinggi+$sisimiring;
    }

 }

 class jajarGenjang implements abs_bangunDatar2{
    function hitungLuas($alas, $tinggi){
        return $alas*$tinggi;
    }

    function hitungKeliling($alas, $tinggi){
        return 2* ($alas+$tinggi);
    }
 }

 class BelahKetupat implements abs_bangunDatar2{
    function hitungLuas($d1, $d2){
        return $d2 * $d1 / 2;
    }

    function hitungKeliling($d1, $d2){
        $d1_bagi2 = $d1 / 2;
        $d2_bagi2 = $d2 / 2;
        $sisimiring = sqrt(pow($d1_bagi2, 2) + pow($d2_bagi2, 2));
        return 4 * ($sisimiring);
    }
 }

 class tampilHasil{
    function tampilSegitiga($alas, $tinggi){
        $segitiga = new segitiga();
        echo "Luas segitiga adalah " .$segitiga->hitungLuas($alas, $tinggi);
        echo " Keliling segitiga adalah ". $segitiga->hitungKeliling($alas, $tinggi)."<br>";
    }

    function tampilPersegi($sisi){
        $persegi = new Persegi();
        echo "Luas persegi adalah " .$persegi->hitungLuas($sisi);
        echo " Keliling persegi adalah ". $persegi->hitungKeliling($sisi)."<br>";
    }
    
    function tampilPersegiPanjang($panjang, $lebar){
        $persegiPanjang = new PersegiPanjang();
        echo "Luas persegi panjang adalah " .$persegiPanjang->hitungLuas($panjang, $lebar);
        echo " Keliling persegi panjang adalah ". $persegiPanjang->hitungKeliling($panjang, $lebar)."<br>";
    }

    function tampilTrapesium($atas, $bawah, $tinggi){
        $trapesium = new Trapesium();
        echo "Luas Trapesium adalah " .$trapesium->hitungLuas($atas, $bawah, $tinggi);
        echo " Keliling Trapesium adalah ". $trapesium->hitungKeliling($atas, $bawah, $tinggi)."<br>";
    }

    function tampilJajarGenjang($alas, $tinggi){
        $jajarGenjang = new jajarGenjang();
        echo "Luas jajar genjang adalah " .$jajarGenjang->hitungLuas($alas, $tinggi);
        echo " Keliling jajar genjang adalah ". $jajarGenjang->hitungKeliling($alas, $tinggi)."<br>";
    }

    function tampilBelahKetupat($d1, $d2){
        $belahKetupat = new belahKetupat();
        echo "Luas belah ketupat adalah " .$belahKetupat->hitungLuas($d1, $d2);
        echo " Keliling belah ketupat adalah ". $belahKetupat->hitungKeliling($d1, $d2)."<br>";
    }

    function tampilLingkaran($jarijari){
        $Lingkaran = new Lingkaran();
        echo "Luas lingkaran adalah " .$Lingkaran->hitungLuas($jarijari);
        echo " Keliling lingkaran adalah ". $Lingkaran->hitungKeliling($jarijari)."<br>";
    }
 }

 $tampilHasil = new tampilHasil();
 $tampilHasil->tampilPersegi(3);
 $tampilHasil->tampilLingkaran(3);
 $tampilHasil->tampilPersegiPanjang(3,4);
 $tampilHasil->tampilSegitiga(3,4);
 $tampilHasil->tampilJajarGenjang(3,4);
 $tampilHasil->tampilBelahKetupat(3,4);
 $tampilHasil->tampilTrapesium(3,4,5);
?>